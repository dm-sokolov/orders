﻿namespace Orders.Core.Models
{
    public class BaseModel
    {
        public virtual int Id { get; set; }
    }
}
