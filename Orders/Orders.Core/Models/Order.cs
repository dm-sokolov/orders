﻿using System;
using Orders.Core.Enums;

namespace Orders.Core.Models
{
    public class Order : BaseModel 
    {
        public virtual DateTime CreatedOn { get; set; }
        public virtual string Name { get; set; }
        public virtual OrderStatus OrderStatus { get; set; }
    }
}
