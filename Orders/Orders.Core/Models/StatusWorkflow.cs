﻿using Orders.Core.Enums;

namespace Orders.Core.Models
{
    public class StatusWorkflow : BaseModel
    {
        public virtual OrderStatus? StatusFrom { get; set; }
        public virtual OrderStatus? StatusTo { get; set; }
    }
}
