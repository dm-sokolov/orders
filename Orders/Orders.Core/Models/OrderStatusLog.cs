﻿using System;
using Orders.Core.Enums;

namespace Orders.Core.Models
{
    public class OrderStatusLog : BaseModel
    {
        public virtual DateTime ChangedOn { get; set; }
        public virtual OrderStatus OldStatus { get; set; }
        public virtual OrderStatus NewStatus { get; set; }
        public virtual Order Order { get; set; }
    }
}
