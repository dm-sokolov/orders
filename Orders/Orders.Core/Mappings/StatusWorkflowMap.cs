﻿using FluentNHibernate.Mapping;
using Orders.Core.Enums;
using Orders.Core.Models;

namespace Orders.Core.Mappings
{
    public class StatusWorkflowMap : ClassMap<StatusWorkflow>
    {
        public StatusWorkflowMap()
        {
            Table("status_workflow");
            Id(i => i.Id, "id")
                .GeneratedBy.Native();
            Map(i => i.StatusFrom, "status_from")
                .CustomType(typeof(OrderStatus));
            Map(i => i.StatusTo, "status_to")
                .CustomType(typeof(OrderStatus));
        }
    }
}
