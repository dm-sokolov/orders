﻿using FluentNHibernate.Mapping;
using Orders.Core.Enums;
using Orders.Core.Models;


namespace Orders.Core.Mappings
{
    public class OrderMap : ClassMap<Order>
    {
        public OrderMap()
        {
            //Переименовано т.к. "order" зарезервированное слово в sql
            Table("orders");
            Id(i => i.Id, "id")
                .GeneratedBy.Native();
            Map(i => i.CreatedOn, "created_on");
            Map(i => i.Name, "name");
            Map(i => i.OrderStatus, "order_status")
                .CustomType(typeof(OrderStatus));
        }
    }
}
