﻿using FluentNHibernate.Mapping;
using Orders.Core.Enums;
using Orders.Core.Models;

namespace Orders.Core.Mappings
{
    public class OrderStatusLogMap : ClassMap<OrderStatusLog>
    {
        public OrderStatusLogMap()
        {
            Table("order_status_log");
            Id(i => i.Id, "id")
                .GeneratedBy.Native();
            Map(i => i.ChangedOn, "changed_on");
            Map(i => i.OldStatus, "old_status")
                .CustomType(typeof(OrderStatus));
            Map(i => i.NewStatus, "new_status")
                .CustomType(typeof(OrderStatus));
            References(i => i.Order, "order_id")
                .Cascade.None();
        }
    }
}
