﻿using System;
using System.ComponentModel;

namespace Orders.Core.Extentions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum enumElement)
        {
            if (enumElement == null)
                return string.Empty;

            var type = enumElement.GetType();

            var memInfo = type.GetMember(enumElement.ToString());
            if (memInfo?.Length > 0)
            {
                var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs?.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return enumElement.ToString();
        }
    }
}
