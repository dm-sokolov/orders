﻿using System.ComponentModel;

namespace Orders.Core.Enums
{
    /// <summary>
    /// Статус заявки
    /// </summary>
    public enum OrderStatus
    {

        [Description("Открыта")]
        Open = 0,

        [Description("Решена")]
        Resolved = 1,

        [Description("Возврат")]
        Return = 2,

        [Description("Закрыта")]
        Closed = 3
    }
}