﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Orders.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                if (context.Response.HasStarted)
                    throw;
                var str = new StringBuilder();
                str.AppendLine($"Message {exception.Message}");
                str.AppendLine($"Stack trace {exception.StackTrace}");
                var ex = exception.InnerException;
                while (ex != null)
                {
                    str.AppendLine("Inner exception");
                    str.AppendLine($"Message {ex.Message}");
                    str.AppendLine($"Stack trace {ex.StackTrace}");
                    ex = ex.InnerException;
                }
                context.Response.Clear();
                context.Response.Redirect("/Home/Error");
                return;
            }
        }
    }
}
