﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Microsoft.Extensions.Options;
using NHibernate;
using NHibernate.Context;
using NHibernate.Caches.CoreMemoryCache;
using NHibernate.Tool.hbm2ddl;
using Orders.Config;

namespace Orders.DAL
{
    public class AppSessionFactory
    {
        private readonly DatabaseConfig _databaseConfig; 
        private ISessionFactory _sessionFactory;
        private readonly object _lock = new object();

        public AppSessionFactory(IOptions<DatabaseConfig> databaseConfig)
        {
            _databaseConfig = databaseConfig.Value;
        }

        public ISession OpenSession()
        {
            var factory = GetSessionFactory();
            var session = factory.OpenSession();
            session.BeginTransaction();

            return session;
        }

        private ISessionFactory GetSessionFactory()
        {
            lock (_lock)
            {
                if (_sessionFactory == null)
                    CreateSessionFactory();
            }

            return _sessionFactory;
        }

        private void CreateSessionFactory()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var mappingAssembly = assemblies.FirstOrDefault(i => i.FullName.StartsWith("Orders.Core"));
            var configuration = GetConfiguration(_databaseConfig.Sql);
            _sessionFactory = BuildSessionFactory(configuration, mappingAssembly);
        }

        private ISessionFactory BuildSessionFactory(FluentConfiguration config, Assembly mappingAssembly)
        {
            var cfg = config
                .Mappings(m => m.FluentMappings.AddFromAssembly(mappingAssembly))
                .BuildConfiguration();

            var exporter = new SchemaExport(cfg);
            exporter.Execute(true, true, false);

            return cfg.BuildSessionFactory();
        }

        private FluentConfiguration GetConfiguration(string connectionKey)
        {
            var cfg = Fluently.Configure()
                .Database(MsSqlConfiguration
                    .MsSql2012
                    .ConnectionString(e => e.Is(connectionKey))
                    .UseReflectionOptimizer()
                    .Raw(NHibernate.Cfg.Environment.CommandTimeout, TimeSpan.FromMinutes(5)
                        .TotalSeconds.ToString(CultureInfo.InvariantCulture))
                    .AdoNetBatchSize(100))
                .Cache(c => c
                    .UseQueryCache()
                    .UseSecondLevelCache()
                    .ProviderClass<CoreMemoryCacheProvider>())
                .CurrentSessionContext<AsyncLocalSessionContext>();

            if (_databaseConfig.ShowSql)
            {
                cfg.Database(MsSqlConfiguration
                    .MsSql2012
                    .FormatSql()
                    .ShowSql()
                    .Raw(NHibernate.Cfg.Environment.GenerateStatistics, "true"));
            }

            return cfg;
        }
    }
}
