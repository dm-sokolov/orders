﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;
using Orders.AutoMapper.Extentions;
using Orders.Controllers.Base;
using Orders.Core.Models;
using Orders.DTO;
using Orders.Repository;

namespace Orders.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderStatusLog> _orderStatusLogRepository;
        private readonly IMapper _mapper;

        public OrderController(IMapper mapper,
                                IRepository<Order> orderRepository,
                                IRepository<OrderStatusLog> orderStatusLogRepository)
        {
            _orderRepository = orderRepository;
            _orderStatusLogRepository = orderStatusLogRepository;
            _mapper = mapper;
        }

        //public IActionResult Orders() => View();

        [HttpPost]
        public async Task<ActionResult> GetOrders([DataSourceRequest] DataSourceRequest request)
        {
            var orders = await _orderRepository.Query().ToListAsync();
            var dtos = orders.MapTo<List<OrderDto>>(_mapper);
            return Json(dtos.ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<IActionResult> Put(OrderDto orderDto)
        {
            var newOrder = orderDto.MapTo<Order>(_mapper);

            var result = await _orderRepository.SaveAsync(newOrder);
            return Json(result);
    }
}
}