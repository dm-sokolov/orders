﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Orders.Code.ActionFilters;

namespace Orders.Controllers.Base
{
    [UserViewBag]
    public class BaseController : Controller
    {
        public ViewResult View(IMapper mapper, object model, Type destinationType)
        {
            var sourceType = model.GetType();
            var dto = mapper.Map(model, sourceType, destinationType);
            return View(dto);
        }

        public JsonResult Json(IMapper mapper, object model, Type destinationType)
        {
            var sourceType = model.GetType();
            var dto = mapper.Map(model, sourceType, destinationType);
            return Json(dto);
        }
    }
}
