﻿using System;
using System.Linq;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Orders.Config;
using Orders.DAL;
using Orders.Middlewares;
using Orders.Repository;

namespace Orders
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Add framework services.
           services
               .AddControllersWithViews()

               // Maintain property names during serialization. See:
               // https://github.com/aspnet/Announcements/issues/194
               .AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());


            services.AddSingleton<AppSessionFactory>();
            services.AddScoped(x => x.GetRequiredService<AppSessionFactory>()
                                                            .OpenSession());
            services.Configure<DatabaseConfig>(Configuration.GetSection("Database"));

            services.AddKendo();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseMiddleware<CloseSessionMiddleware>();
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.AddRepositories();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(i => i.FullName.StartsWith("Orders")).ToArray();
            builder.RegisterAssemblyTypes(assemblies)
                .Where(i => !string.IsNullOrEmpty(i.Namespace) && (i.Namespace.EndsWith(".Helpers") || i.Namespace.EndsWith(".Services")))
                .AsImplementedInterfaces();
        }
    }
}