﻿using AutoMapper;
using Orders.Core.Models;
using Orders.DTO;
using Utils.Kendo.Extensions;

namespace Orders.AutoMapper.Profiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderDto>();
            CreateMap<OrderDto, Order>();
        }
    }
}
