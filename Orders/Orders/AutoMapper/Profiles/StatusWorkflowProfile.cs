﻿using AutoMapper;
using Orders.Core.Models;
using Orders.DTO;

namespace Orders.AutoMapper.Profiles
{
    public class StatusWorkflowProfile : Profile
    {
        public StatusWorkflowProfile()
        {
            CreateMap<StatusWorkflow, StatusWorkflowDto>();
            CreateMap<StatusWorkflowDto, StatusWorkflow>();
        }
    }
}
