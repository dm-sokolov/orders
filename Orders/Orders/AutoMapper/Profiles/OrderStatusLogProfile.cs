﻿using AutoMapper;
using Orders.Core.Models;
using Orders.DTO;
using Utils.Kendo.Extensions;

namespace Orders.AutoMapper.Profiles
{
    public class OrderStatusLogProfile : Profile
    {
        public OrderStatusLogProfile()
        {
            CreateMap<OrderStatusLog, OrderStatusLogDto>();
            CreateMap<OrderStatusLogDto, OrderStatusLog>();
        }
    }
}
