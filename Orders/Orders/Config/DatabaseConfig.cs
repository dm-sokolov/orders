﻿namespace Orders.Config
{
    public class DatabaseConfig
    {
        public string Sql { get; set; }

        public bool ShowSql { get; set; }
    }
}
