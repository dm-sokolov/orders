﻿using System;
using Kendo.Mvc.UI.Fluent;
using Orders.Core.Extentions;
using Utils.Kendo.Extensions;

namespace Utils.Kendo.Extentions
{
    public static class ComboBoxExtensions
    {
        //public static ComboBoxBuilder FillFromEnum<TEnum>(this ComboBoxBuilder combobox) where TEnum : Enum
        //{
        //    return combobox.DataSource(ds => ds.Read(r => r.Action("GetForCombobox", "Enum")
        //        .Data($"()=> ({{ name: '{typeof(TEnum).Name}' }})")));
        //}

        /// <summary>
        /// Заполнить комбобокс из енама
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dd"></param>
        /// <returns></returns>
        public static ComboBoxBuilder FillItemsFromEnum<T>(this ComboBoxBuilder dd)
            where T : Enum
        {
            return dd.Items(i =>
            {
                foreach (Enum? value in Enum.GetValues(typeof(T)))
                {
                    i.Add()
                        .Text(value.GetDescription())
                        .Value(Convert.ToInt32(value)
                            .ToString());
                }
            });
        }
    }
}
