﻿using Orders.Core.Enums;
using Orders.DTO.Base;

namespace Orders.DTO
{
    public class StatusWorkflowDto : BaseDto
    {
        public OrderStatus? StatusFrom { get; set; }
        public OrderStatus? StatusTo { get; set; }
    }
}
