﻿using System;
using System.ComponentModel;
using Orders.Core.Enums;
using Orders.Core.Extentions;
using Orders.DTO.Base;

namespace Orders.DTO
{
    public class OrderStatusLogDto : BaseDto
    {
        [DisplayName("Дата изменения")]
        public DateTime ChangedOn { get; set; }

        public OrderStatus OldStatus { get; set; }

        [DisplayName("Начальный статус")] 
        public string OldStatusToString => OldStatus.GetDescription();

        public OrderStatus NewStatus { get; set; }

        [DisplayName("Конечный статус")]
        public string NewStatusToString => NewStatus.GetDescription();
    }
}
