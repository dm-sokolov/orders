﻿using System;
using System.ComponentModel;
using Orders.Core.Enums;
using Orders.Core.Extentions;
using Orders.DTO.Base;

namespace Orders.DTO
{
    public class OrderDto : BaseDto
    {
        [DisplayName("Дата создания")]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Название")]
        public string Name { get; set; }

        public OrderStatus OrderStatus { get; set; }

        [DisplayName("Статус")] 
        public string OrderStatusToString => OrderStatus.GetDescription();
    }
}
